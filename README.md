[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/kylesferrazza)](https://artifacthub.io/packages/search?repo=kylesferrazza)

# Using the helm repository

```
helm repo add kylesferrazza https://charts.kylesferrazza.com
helm repo update
helm search repo kylesferrazza
```
